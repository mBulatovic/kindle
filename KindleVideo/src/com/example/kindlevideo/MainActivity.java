package com.example.kindlevideo;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends Activity implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
	public static final String TAG = "MyVideoPlayer";
	private VideoView mVideoView;
	private Uri mUri;
	private int mPositionWhenPaused = -1;

	private MediaController mMediaController;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		//Set the screen to landscape.
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		mVideoView = (VideoView) findViewById(R.id.video_view);
		
		//Video file
		mUri = Uri.parse("http://www.androidbegin.com/tutorial/AndroidCommercial.3gp");
		
		
		mMediaController = new MediaController(this);
		mVideoView.setMediaController(mMediaController);
	}

	public void onStart() {
		// Play Video
		mVideoView.setVideoURI(mUri);
		mVideoView.start();
		
		super.onStart();
	}
	
	public void onPause() {
		// Stop video when the activity is pause.
		mPositionWhenPaused = mVideoView.getCurrentPosition();
		mVideoView.stopPlayback();
		
		super.onPause();
	}
	
	public void onResume() {
		// Resume video player
		if (mPositionWhenPaused >= 0) {
			mVideoView.seekTo(mPositionWhenPaused);
			mPositionWhenPaused = -1;
		}

		super.onResume();
	}
	
	public boolean onError(MediaPlayer player, int arg1, int arg2) {
		return false;
	}
	
	public void onCompletion(MediaPlayer mp) {
		this.finish();
	}
}
